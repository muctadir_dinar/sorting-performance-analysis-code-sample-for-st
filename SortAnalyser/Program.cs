﻿using SortAnalyser.Sorters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SortAnalyser
{
    class Program
    {
        static void Main(string[] args)
        {
            var unsorted = GenerateRandomList(1000).ToList();
            var sorters = new List<AbsSorter<int>>
            {
                new BubbleSorter<int>(),
                new InsertionSort<int>(),
                new MergeSort<int>(),
                new QuickSort<int>()
            };

            var analysis = new SortTimeCalculator().Analyze(sorters, unsorted);
            foreach (var kv in analysis)
            {
                Console.WriteLine($"{kv.Key.GetType().Name}\t: {kv.Value} ms");
            }
            Console.ReadKey();
        }


        private static IEnumerable<int> GenerateRandomList(int size)
        {
            var random = new Random();
            for (var i = 0; i < size; i++)
            { yield return random.Next(); }
        }
    }
}
