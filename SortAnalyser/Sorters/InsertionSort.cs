﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortAnalyser.Sorters
{
    public class InsertionSort<T> : AbsSorter<T> where T : IComparable
    {
        protected override IEnumerable<T> SortImpl(List<T> unsorted)
        {
            var sorted = unsorted.ToList();
            for (var i = 0; i < sorted.Count; i++)
            {
                var j = i;
                var current = sorted[i];
                for (; j > 0; j--)
                {
                    if (sorted[j - 1].CompareTo(current) > 0)
                    {
                        sorted[j] = sorted[j - 1];
                    }
                    else break;
                }
                sorted[j] = current;
            }
            return sorted;
        }
    }
}