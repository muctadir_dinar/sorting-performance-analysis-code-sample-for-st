﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortAnalyser.Sorters
{
    public class BubbleSorter<T> : AbsSorter<T> where T : IComparable
    {
        protected override IEnumerable<T> SortImpl(List<T> unsorted)
        {
            var sorted = unsorted.ToList();
            for (var i = 0; i < sorted.Count; i++)
            {
                for (var j = 0; j < sorted.Count - 1; j++)
                {
                    if (sorted[j].CompareTo(sorted[j + 1]) > 0)
                    {
                        var temp = sorted[j + 1];
                        sorted[j + 1] = sorted[j];
                        sorted[j] = temp;
                    }
                }
            }
            return sorted;
        }
    }
}