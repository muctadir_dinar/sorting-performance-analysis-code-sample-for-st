﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortAnalyser.Sorters
{
    public abstract class AbsSorter<T> where T : IComparable
    {
        public virtual TimeSpan LastExecutionTime { get; protected set; }

        public virtual IEnumerable<T> Sort(IEnumerable<T> unsorted)
        {
            var unsortedList = PreprocessData(unsorted);
            var startTime = DateTime.Now;
            var sorted = SortImpl(unsortedList);
            LastExecutionTime = DateTime.Now.Subtract(startTime);
            return sorted;
        }

        protected virtual List<T> PreprocessData(IEnumerable<T> data)
        {
            return data.ToList();
        }

        protected abstract IEnumerable<T> SortImpl(List<T> unsorted);
    }
}
