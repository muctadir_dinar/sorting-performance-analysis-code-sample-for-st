﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortAnalyser.Sorters
{
    class MergeSort<T> : AbsSorter<T> where T : IComparable
    {
        protected override IEnumerable<T> SortImpl(List<T> unsortedList)
        {
            var sorted = new List<T>();
            if (unsortedList.Count < 2)
            {
                sorted.AddRange(unsortedList);
            }
            else
            {
                var mid = unsortedList.Count / 2;
                var sortedR = SortImpl(unsortedList.Take(mid).ToList()).ToList();
                var sortedL = SortImpl(unsortedList.Skip(mid).ToList()).ToList();

                int r = 0, l = 0;
                for (var i = 0; i < unsortedList.Count && r < sortedR.Count && l < sortedL.Count; i++)
                {
                    if (sortedR[r].CompareTo(sortedL[l]) < 0)
                    {
                        sorted.Add(sortedR[r]);
                        r++;
                    }
                    else
                    {
                        sorted.Add(sortedL[l]);
                        l++;
                    }
                }
                sorted.AddRange(sortedR.Skip(r));
                sorted.AddRange(sortedL.Skip(l));
            }
            
            return sorted;
        }
    }
}