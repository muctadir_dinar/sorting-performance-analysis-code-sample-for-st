﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SortAnalyser.Sorters
{
    class QuickSort<T> : AbsSorter<T> where T : IComparable
    {
        protected override IEnumerable<T> SortImpl(List<T> unsortedList)
        {
            if (unsortedList.Count < 2) return unsortedList;

            var pivotIdx = new Random().Next(unsortedList.Count());
            var pivot = unsortedList[pivotIdx];

            List<T> smaller = new List<T>(), greater = new List<T>();
            for (var i = 0; i < unsortedList.Count; i++)
            {
                if (i != pivotIdx)
                {
                    if (unsortedList[i].CompareTo(pivot) < 0)
                    {
                        smaller.Add(unsortedList[i]);
                    }
                    else
                    {
                        greater.Add(unsortedList[i]);
                    }
                }
            }

            var sorted = SortImpl(smaller).ToList();
            sorted.Add(pivot);
            sorted.AddRange(SortImpl(greater));
            return sorted;
        }
    }
}