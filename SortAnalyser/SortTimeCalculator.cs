﻿using SortAnalyser.Sorters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SortAnalyser
{
    class SortTimeCalculator
    {
        public Dictionary<AbsSorter<T>, double> Analyze<T>(IEnumerable<AbsSorter<T>> sorters, List<T> unsorted) where T : IComparable
        {
            var performanceMap = new Dictionary<AbsSorter<T>, double>();
            Parallel.ForEach(sorters, sorter =>
            {
                var timeSpans = new List<TimeSpan>();
                for (var i = 0; i < 2000; i++)
                {
                    sorter.Sort(unsorted);
                    timeSpans.Add(sorter.LastExecutionTime);
                }
                performanceMap.Add(sorter, timeSpans.Average(t => t.TotalMilliseconds));
            });
            return performanceMap;
        }
    }
}
